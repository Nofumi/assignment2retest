﻿using System;

namespace BowlingGame
{
    public class Game
    {
        
        int[]pinFalls = new int[21];
        int rollCounter;

        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public void Roll(int pins)
        {
            pinFalls[rollCounter] += pins;
            rollCounter++;
        }

        public int Score()
        {
            int score = 0;
            int i = 0;
            for(int Frame = 0; Frame < 10; Frame++)
            {
                if(pinFalls[i] == 10)
                {
                    score += 10 + pinFalls[i + 1] + pinFalls[i + 2];
                    i += 1;
                }
                else if (pinFalls[i] + pinFalls[i + 1] == 10)
                {
                    score += 10 + pinFalls[i + 2];
                    i += 2;

                }
                else
                {
                    score += pinFalls[i] + pinFalls[i + 1];
                    i += 2;

                }



            }
            return score;

        }

    }
}